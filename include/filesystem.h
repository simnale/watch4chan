#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>

namespace file
{
    void makedir(std::string path, std::string name)
    {
	std::string full = path+name;
	mkdir(full.c_str(), 0777);
    }
    
     void makedir(std::string path)
    {
	makedir(path, "");
    }
}