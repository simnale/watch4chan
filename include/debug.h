#ifndef DEBUG_H
#define DEBUG_H

#define DEBUG(x) do { \
  if (debugging_enabled) {std::cerr<<__FILE__<<"::"<<__LINE__<<" in "<<__func__<<": "<<#x<<" = "<<x<<std::endl;} \
} while (0)

bool debugging_enabled = true;

#endif