#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <debug.h>


//http://boards.4chan.org/g/res/39506659

namespace http
{
    void download_thread(std::string url)
    {
	std::string host {"boards.4chan.org"};
	std::string path;
	
	std::string::size_type n;
	n = url.find("org");
	path = url.substr(n+3);

	
	/* construct http request */
	struct addrinfo req, *res;
	memset(&req, 0, sizeof(req));
	req.ai_family = AF_UNSPEC;
	req.ai_socktype = SOCK_STREAM;
	
	if (getaddrinfo("boards.4chan.org", "http", &req, &res) < 0)
	{
	    std::cerr << "Error at getaddrinfo()";
	    return;
	}
	
	int s = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (s < 0)
	{
	    std::cerr << "Error at socket()";
	    return;
	}
	if (connect(s, res->ai_addr, res->ai_addrlen) < 0)
	{
	    std::cerr << "Error at connect()";
	    return;
	}
	
	std::string get_request = "GET "+path+" HTTP/1.0\r\nHost: "+"boards.4chan.org"+"\r\n\r\n";
	
	if(send(s, get_request.c_str(), strlen(get_request.c_str()), 0) < 0)
	{
	    perror("send");
	    return;
	}
	
	std::ofstream file;
	file.open("/tmp/index.html", std::ios_base::binary);
	
	unsigned char recvbuffer[4];
	int recvcount = 0;
	recvcount = recv(s, recvbuffer, sizeof(recvbuffer), 0);
	
	while (recvcount)
	{
	    file.write(reinterpret_cast<char const*>(recvbuffer),sizeof(recvbuffer));
	    recvcount = recv(s, recvbuffer, sizeof(recvbuffer), 0);
	}
	
	file.write(reinterpret_cast<char const*>(recvbuffer),sizeof(recvbuffer));
	
	freeaddrinfo(res);
	close(s);
    }
}