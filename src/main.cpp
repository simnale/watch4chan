#include <iostream>
#include <vector>
#include <thread>
#include <cstdlib>
#include <debug.h>
#include <http_request.h>
#include <filesystem.h>

std::vector<std::string> watch_list;

void show_list()
{
    std::cout << "Currently you're watching the following threads:\n";
    for (std::size_t t {0}; t < watch_list.size(); ++t)
    {
	std::cout << t << ": " << watch_list[t] << std::endl;
    }
}

void add_to_list()
{
    std::string line;
    std::getline(std::cin, line);
    watch_list.push_back(line);
}

void delete_list_item()
{
    std::size_t index;
    std::cout << "Which thread to unwatch?\n> #";
    std::cin >> index;
    watch_list.erase(watch_list.begin() + index);
    std::string line;
    std::getline(std::cin, line);
}

void create_dir()
{
    std::string line;
    std::getline(std::cin, line);
    file::makedir(line);
}

void listen_to_input()
{
    std::string line;
    while(std::cin)
    {
	std::cout << "> ";
	std::getline(std::cin, line);
	if (line.length() == 1)
	{
	    const char c = line[0];
	    switch(c) {
		case 'l':
		    show_list();
		    break;
		case 'n':
		    add_to_list();
		    break;
		case 'c':
		    return;
		    break;
		case 'd':
		    delete_list_item();
		    break;
		case 't':
		    http::download_thread(watch_list[0]);
		case 'f':
		    create_dir();
		    break;
		default:
		    break;
	    }
	}
    }
}

std::string menu = "-l\tlist threads on watchlist\n-n\tadd new thread\n-d\tdelete thread\n-t\ttest\n-c\tclose";

int main()
{
    std::cout << "Welcome m'lady!" << std::endl;
    std::cout << menu << std::endl;
    std::thread t(listen_to_input);
    t.join();
    return 0;
}